import { GaDeRyPoLuKi } from "./gaderypoluki";
import { equal } from "assert";

// zbiór testów
describe("", () => {
  let gaderypoluki: GaDeRyPoLuKi;

  // przed kazdym testem
  beforeEach(() => {
    gaderypoluki = new GaDeRyPoLuKi();
  });

  // test
  it('should translate "ala" to "gug"', () => {
    const result = gaderypoluki.translate("ala");
    expect(result).toEqual("gug");
  });

  it('should translate "gala" to "agug"', () => {
    const result = gaderypoluki.translate("gala");
    expect(result).toEqual("agug");
  });

  it('should assure "t" is not translatable with "gaderypoluki"', () => {
    const result = gaderypoluki.isTranslatable("t");
    expect(result).toBeFalsy();
  });

  // TODO

  it('should assure "LOK" is not translated', () => {
    const result = gaderypoluki.translate("LOK");
    expect(result).toBeTruthy();
  });

  it('should translate "piotrek" to "okptydi"', () => {
    const result = gaderypoluki.translate("piotrek");
    expect(result).toEqual("okptydi");
  });

  it('should translate "KOT" to "ipt", ignore case', () => {
    const result = gaderypoluki.translateIgnoreCase("KOT");
    expect(result).toEqual("ipt");
  });

  it('should check whether "g" is translatable', () => {
    const result = gaderypoluki.isTranslatable("g");
    expect(result).toBeTruthy();
  });

  it('should assure that "gaderypoluki" key is 12 char long', () => {
    const result = gaderypoluki.keySize;
    expect(result).toEqual(12);
  });
});
