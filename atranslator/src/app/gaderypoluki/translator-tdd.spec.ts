// To jest miejsce od którego należy zacząć zadanie TDD

// Szyfr Cezara z przesunieciem Offset = 3, bez polskich znakow

describe("", () => {
  let cezar: Cezar;

  beforeEach(() => {
    // Uruchamiane przed kazdym testem
    cezar = new Cezar();
  });

  it('should translate "A" to "d"', () => {
    const result = cezar.translate("A");
    expect(result).toEqual("d");
  });

  it('should translate "z" to "c"', () => {
    const result = cezar.translate("z");
    expect(result).toEqual("c");
  });

  it('should translate "zaraz" to "cdudc"', () => {
    const result = cezar.translate("z");
    expect(result).toEqual("c");
  });

  it('should assure "ń" is not translatable with "cesar"', () => {
    const result = cezar.isTranslatable("p");
    expect(result).toBeFalsy();
  });

  it('should translate "piotrek" to "tlsyuhn"', () => {
    const result = cezar.translate("piotrek");
    expect(result).toEqual("okptydi");
  });

  it('should assure that "cesar" offset is 3', () => {
    const result = cezar.offset;
    expect(result).toEqual(3);
  });
});
// Tu sie zastanawiam czy w testach TDD na przyklad w Angularze mamy pisac rowniez ze np udalo sie stoworzyc komponent, albo np nowy serwis. Mozna stworzyc jak w Gaderypoluki nowy komponent o nazwie Cesar i przetestowac. Ale to juz chyba Angular podczas towrzenia automatycznie sprawdza?
// Na przyklad:

describe("CezarComponent", () => {
  let component: CezarComponent;
  let fixture: ComponentFixture<CezarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CezarComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CezarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create component", () => {
    expect(component).toBeTruthy();
  });
});
